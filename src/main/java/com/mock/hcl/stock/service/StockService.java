package com.mock.hcl.stock.service;

import java.util.List;

import com.mock.hcl.stock.model.StockUser;

public interface StockService {

	List<StockUser> getStockUsers();

	StockUser storeQuoteInfo(StockUser stockUser);

}
