package com.mock.hcl.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mock.hcl.stock.model.StockUser;
import com.mock.hcl.stock.repository.StockRepository;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	public StockRepository stockRepository;
	@Override
	public List<StockUser> getStockUsers() {
		
		
		return stockRepository.findAll();
	}
	@Override
	public StockUser storeQuoteInfo(StockUser stockUser) {
		return stockRepository.saveAndFlush(stockUser);
	}

}
