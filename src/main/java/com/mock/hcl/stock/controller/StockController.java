package com.mock.hcl.stock.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.hcl.stock.model.StockUser;
import com.mock.hcl.stock.service.StockService;

@RestController
@RequestMapping(value="stock/api/v1")
public class StockController {

	@Autowired
	public StockService stockService;
	
	@GetMapping("/users")
	public ResponseEntity<List<StockUser>> getUsers(){
		return new ResponseEntity<List<StockUser>>(stockService.getStockUsers(),HttpStatus.OK);
	}
	
	@PostMapping("/quoteinfo")
	public ResponseEntity<StockUser> storeQuoteInfo(@RequestBody StockUser stockUser){
		
		return new ResponseEntity<StockUser>(stockService.storeQuoteInfo(stockUser),HttpStatus.OK);
	}
}
