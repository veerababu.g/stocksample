package com.mock.hcl.stock.controller;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mock.hcl.stock.Constants;

public class MyApp {

	public static void main(String[] args) throws KeyManagementException, KeyStoreException, NoSuchAlgorithmException {
	
		RestTemplate restTemplate = restTemplate();
		
		ObjectMapper mapper= new ObjectMapper();
		 restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		
		HttpEntity<String> request = new HttpEntity<>(new String(), headers);
		
		ResponseEntity<String> response = restTemplate
		            .exchange(Constants.APIURL, HttpMethod.GET, request, String.class);
		System.out.println(("*****************ApiResponse*********"+response));
		
	}
	public static RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
		       @Override public boolean isTrusted(X509Certificate[] x509Certificates, String s)
		                       throws CertificateException {
		           return true;
		       }
		   };

		   SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
		                   .loadTrustMaterial(null, acceptingTrustStrategy)
		                   .build();

		   SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		   CloseableHttpClient httpClient = HttpClients.custom()
		                   .setSSLSocketFactory(csf)
		                   .build();

		   HttpComponentsClientHttpRequestFactory requestFactory =
		                   new HttpComponentsClientHttpRequestFactory();

		   requestFactory.setHttpClient(httpClient);
		   return new RestTemplate(requestFactory);
		   
		}
}
