package com.mock.hcl.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mock.hcl.stock.model.StockUser;

@Repository
public interface StockRepository extends JpaRepository<StockUser, Long> {

}
