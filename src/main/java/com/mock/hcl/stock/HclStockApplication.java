package com.mock.hcl.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HclStockApplication {

	public static void main(String[] args) {
		SpringApplication.run(HclStockApplication.class, args);
	}
}
