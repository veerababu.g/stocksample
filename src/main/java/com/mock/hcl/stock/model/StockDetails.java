package com.mock.hcl.stock.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name="stock_details")
public class StockDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="user_id")
	private Long userId;
	
	@Column(name="trade_name")
	@JsonProperty("TradeName")
	private String tradeName;
	
	@Column(name="trade_price")
	@JsonProperty("TradePrice")
	private Double tradePrice;
	
	@Column(name="total_stock_price")
	@JsonProperty("TotalStockPrice")
	private Double totalStockPrice;
	
	@Column(name="total_fee")
	@JsonProperty("TotalFee")
	private Double totalFee;
	
	@Column(name="total_price")
	@JsonProperty("TotalPrice")
	private Double totalPrice;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="user_id" ,referencedColumnName="user_id", insertable=false, updatable=false)
		})
	private StockUser stockUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}



	public Double getTotalStockPrice() {
		return totalStockPrice;
	}

	public void setTotalStockPrice(Double totalStockPrice) {
		this.totalStockPrice = totalStockPrice;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public StockUser getStockUser() {
		return stockUser;
	}

	public void setStockUser(StockUser stockUser) {
		this.stockUser = stockUser;
	}

	public Double getTradePrice() {
		return tradePrice;
	}

	public void setTradePrice(Double tradePrice) {
		this.tradePrice = tradePrice;
	}
	
	
	
}
