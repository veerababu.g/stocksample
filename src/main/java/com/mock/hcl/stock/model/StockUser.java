package com.mock.hcl.stock.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name="stock_user")
public class StockUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	@JsonProperty("UserId")
	@JsonInclude(Include.NON_NULL)
	private Long userId;
	
	@Column(name="user_name")
	@JsonProperty("UserName")
	private String name;
	
	@OneToMany(targetEntity=StockDetails.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="user_id",referencedColumnName="user_id")
	@JsonProperty("StockDetails")
	private Set<StockDetails> stockDetails;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<StockDetails> getStockDetails() {
		return stockDetails;
	}

	public void setStockDetails(Set<StockDetails> stockDetails) {
		this.stockDetails = stockDetails;
	}
	
	
}
